Rails.application.routes.draw do
  resources :posts do
    member do 
      put 'like' => 'posts#like'
    end
  end
  root 'static_pages#home'

  resources :users, except: [:new]

  get '/cadastro' , to: 'users#new'
  get '/friends' , to: 'users#showFriends'
  post '/friendships' , to: 'users#createFriendship' , as:'friendships'
  get '/lista-usuarios' , to: 'users#index'

  get '/login' , to: 'sessions#new'
  post'/login' , to: 'sessions#create'
  delete '/logout' , to: 'sessions#destroy'

  post '/comments' , to: 'posts#createComment'

end
