class User < ApplicationRecord
    acts_as_voter
    has_secure_password
    validates :password, presence: true, length: {minimum: 6}
    has_many :posts
    has_many :comments
    has_and_belongs_to_many :friendships
end
