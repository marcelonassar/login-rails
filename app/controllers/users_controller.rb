class UsersController < ApplicationController

    before_action :impede_nao_logado, except: [:new, :create]
    before_action :impede_logado, only: [:new]

    def new
        @user = User.new
    end

    def create
        @user = User.new(user_params)
        if @user.save
            log_in @user
            redirect_to @user
        else
            render 'new'
        end
    end

    def index
        @users = User.all
    end


    def show
        @user = User.find(params[:id])
        @posts = Post.where(user: current_user)
        @frindship = Friendship.new
    end

    #friendships
    def showFriends
        @friends = Friendship.where(user: current_user)
        #friends_names = Array.new
        #friends.each do |friend|
        #    f_id = friend.friend_user_id
        #    f_name = User.find(f_id)
        #    friends_names.push(f_name)
        #end

        #@friends = friends_names
    end

    def createFriendship
        @friendship = Friendship.new(friendship_params)
        @friendship.save
    end

    private
    def user_params
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def impede_nao_logado
        if !logged_in?
            redirect_to root_path
        end
    end

    def friendship_params
        params.require(:friendship).permit(:user_id, :friend_user_id)
    end

    def reverse_friendship_params
        params.require(:friendship).permit(:friend_user_id, :user_id)
    end

    def impede_logado
        if logged_in?
            redirect_to current_user
        end
    end
end
